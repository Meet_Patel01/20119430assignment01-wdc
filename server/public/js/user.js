const users = [
    {
       uid: 001,
       email: 'john@dev.com',
       personalInfo: {
          name: 'John',
          address: {
             line1: 'westwish st',
             line2: 'washmasher',
             city: 'wallas',
             state: 'WX'
          }
       }
    },
    {
       uid: 063,
       email: 'a.abken@larobe.edu.au',
       personalInfo: {
          name: 'amin',
          address: {
             line1: 'Heidelberg',
             line2: '',
             city: 'Melbourne',
             state: 'VIC'
          }
       }
    },
    {
       uid: 045,
       email: 'Linda.Paterson@gmail.com',
       personalInfo: {
          name: 'Linda',
          address: {
             line1: 'Cherry st',
             line2: 'Kangaroo Point',
             city: 'Brisbane',
             state: 'QLD'
          }
       }
    }
 ];

 function returnUsers(users) {
    let result = [];
    let name;
    let email;
    let state;
    users.forEach(element => {
        name = element.personalInfo.name;
        email = element.email;
        state = element.personalInfo.address.state;
        result.push({name, email, state});
    });
     return result;
 }

 function createUserTable(data) {
    var dataString = "";
    for (i = 0; i < data.length; i++) {
        dataString = dataString + "<tr>";
        dataString = dataString + "<td>" + data[i].name + "</td>";
        dataString = dataString + "<td>" + data[i].email + "</td>";
        dataString = dataString + "<td>" + data[i].state + "</td>";
        dataString = dataString + "</tr>";                
    }    

    var content ="<table cellspacing='2' cellpadding='2' border='2px'>";
    content = content + "<tr>";
    content = content + "<th>Name</th>";
    content = content + "<th>Email</th>";
    content = content + "<th>State</th>";
    content = content + "</tr>";
    content = content + dataString;    
    content = content + "</table>";
    return content;
}

document.getElementById("button-show-users").addEventListener('click', () => {
    var data = returnUsers(users);   
    document.getElementById('loaduser').innerHTML = createUserTable(data);
});

document.getElementById("button-short-users").addEventListener('click', () => {
    var data = returnUsers(users);  
    data.sort((a, b) => ((a.name).toLowerCase() > (b.name).toLowerCase()) ? 1 : -1);
    document.getElementById('loaduser').innerHTML = createUserTable(data);
});