
const path = require("path");
const _ = require('lodash');
const express = require("express");
const app = express(); // create express app

app.use(express.static("public"));
// app.use(express.static(path.join(__dirname, "public")));


// app.get("/", (req, res) => {
//     res.sendFile(path.join(__dirname, "public", "index.html"));
// });

// start express server on port 4548
app.listen(4548, () => {
  console.log("server started on port 4548");
});